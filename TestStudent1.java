/*
 * FInn etternavn??
 * Alt. b
 */

class Student1{
	private Name name;
	private int bday;

	public Student1(Name name, int bday){
		// this.name = name;
		this.name = new Name(name.getFirstname(), name.getSurname());
		this.bday = bday;
	}

	public Name getName(){
		return new Name(name.getFirstname(), name.getSurname());
		//return name;
	}

	public String getFirstname(){
		return name.getFirstname();
	}

	public String getSurname(){  return name.getSurname();}

	public int getBday(){
		return bday;
	}

	public  void setFDato(int newValue){
		this.bday = newValue;
	}

	public void setSurname(String newValue){
		name.setSurname(newValue);
	}

	public void setFirstname(String newValue){
		name.setFirstname(newValue);
	}

	public String toString(){
		return name + " " + bday;
	}
}

class TestStudent1{
	public static void main(String[] args){

		Name name = new Name("Grethe", "Olsen");

		Student1 studentA = new Student1(name, 19801010);

		studentA.setFirstname("Oskar");
		System.out.println("navn: " + name);
		System.out.println("studentA.navn: " + studentA);

		name = studentA.getName();
		name.setFirstname("Stine");
		System.out.println("studentA.navn: " + studentA);
		System.out.println("navn: " + name);

		name.setFirstname("Roger");
		System.out.println("navn: " + name);

		Name studentB = new Name("Roger","Olsen");
		System.out.println(studentB.equals(name));
		

	}



}

