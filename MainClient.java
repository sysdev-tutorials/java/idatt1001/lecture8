
public class MainClient {
    public static void main(String[] args) {
        new MainClient().oppgave1();
        new MainClient().oppgave2();
        new MainClient().oppgave3_1();
        new MainClient().oppgave3_2();
        new MainClient().oppgave3_3();
        new MainClient().oppgave4();
    }

    private void oppgave1(){
        System.out.println("Oppgave 1");
        int tall1 = 3;
        int tall2 =4;
        System.out.println("Før: " + tall1 + " " + tall2);
        int hjelp = tall1;
        tall1 = tall2;
        tall2 = hjelp;
        System.out.println("Etter: " + tall1 + " " + tall2);
    }
    
    private void oppgave2(){
        System.out.println("Oppgave 2");
        RubbishAndNonsense rubbishAndNonsense = new RubbishAndNonsense();
        int a = 10, b = 20;
        System.out.println("Før: " + a + " " + b);
        rubbishAndNonsense.swapNumbers(a, b);
        System.out.println("Før: " + a + " " + b);
    } 

    private void oppgave3_1(){
        System.out.println("oppgave3_1");
        RubbishAndNonsense rubbishAndNonsense = new RubbishAndNonsense();
        Name name1 = new Name("Ole", "Normann");
        Name name2 = new Name("Kari", "Grossmann");

        System.out.println("Før: " + name1 + " " + name2);
        rubbishAndNonsense.swapObjects1(name1, name2);
        System.out.println("Før: " + name1 + " " + name2);
    }

    private void oppgave3_2(){
        System.out.println("oppgave3_2");
        RubbishAndNonsense rubbishAndNonsense = new RubbishAndNonsense();
        Name name1 = new Name("Ole", "Normann");
        Name name2 = new Name("Kari", "Grossmann");

        System.out.println("Før: " + name1 + " " + name2);
        rubbishAndNonsense.swapObjects2(name1, name2);
        System.out.println("Før: " + name1 + " " + name2);
    }

    private void oppgave3_3(){
        System.out.println("oppgave3_3");
        RubbishAndNonsense rubbishAndNonsense = new RubbishAndNonsense();
        Name name1 = new Name("Ole", "Normann");
        Name name2 = new Name("Kari", "Grossmann");

        System.out.println("Før: " + name1 + " " + name2);
        rubbishAndNonsense.swapObjects3(name1, name2);
        System.out.println("Før: " + name1 + " " + name2);
    }

    private void oppgave4(){
        System.out.println("oppgave4");
        RubbishAndNonsense rubbishAndNonsense = new RubbishAndNonsense();
        Name name1 = new Name("Ole", "Normann");
        Name name2 = new Name("Kari", "Grossmann");

        System.out.println("Før: " + name1 + " " + name2);
        rubbishAndNonsense.swapObjects4(name1, name2);
        System.out.println("Før: " + name1 + " " + name2);
    } 


}

