class Person {
    
    private final String name;
    private final int yearOfBirth;

    public Person (String name, int yearOfBirth){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    // 1
    public boolean isSameAge(Person anotherPerson){
        return yearOfBirth == anotherPerson.yearOfBirth;
    }

    //2 
    public int compareTo(Person anothPerson) {
        if(yearOfBirth < anothPerson.yearOfBirth) return -1;
        else if (yearOfBirth == anothPerson.yearOfBirth) return 0;
        else return 1;
    }

    // 3
    public boolean olderThan(int age, Person anotherPerson) {
        return yearOfBirth - age > anotherPerson.yearOfBirth;
    }

    // 4
    @Override
    public boolean equals(Object anotherPerson) {

        if(!(anotherPerson instanceof Person)) return false;

        if(this == anotherPerson) return true;

        if(name == ((Person)anotherPerson).name && yearOfBirth == ((Person)anotherPerson).yearOfBirth) {
            return true;
        }

        return false;
    }
}

class PersonClient {
    public static void main(String[] args) {
        new PersonClient().oggave5();
    }

    private void oggave5(){
        Person p1 = new Person("Ole", 1980);
        Person p2 = new Person("Kari", 1984);

        System.out.println("p1 and p1 have age: " + p1.isSameAge(p2));
        System.out.println("p1 is older than p2: " + p1.compareTo(p2));
        System.out.println("p1 is 4 years older than p2: " + p1.olderThan(4, p2));
        System.out.println("p1 and p2 are equal: " + p1.equals(p2));
    }
}
