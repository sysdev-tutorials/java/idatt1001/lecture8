// Flere referanser har tilgang til de samme objektene

class Student4 {
    private Name name;
    private int bday;
    public Student4(Name name, int bday) {
      this.name = name;  // lager ikke kopi, skummelt!
      this.bday = bday;
    }
    public int getBday() {
      return bday;
    }
    public Name getName() {
      return name; // lager ikke kopi, skummelt!
    }
    public void setName(Name newValue) {
      name = newValue;  // lager ikke kopi, skummelt!
    }
    public String toString() {
      return name + ", født: " + bday;
    }
  }
  
  class TestStudent4 {
    public static void main(String[] args) {
      Name studName = new Name("Ole Andreas", "Thomassen");
      Student4 student = new Student4(studName, 19801010);
      System.out.println("A: Student " + student);
  
      studName.setFirstname("Ingolf");
      System.out.println("B: Student " + student);
  
      Name myName = student.getName();
      myName.setFirstname("Kåre");
      System.out.println("C: Student " + student);
  
      student.setName(new Name("Ole Andreas", "Haug"));
      System.out.println("D: Student " + student);
    }
  }
  
  /*
  A: Student Ole Andreas Thomassen, født: 19801010
  B: Student Ingolf Thomassen, født: 19801010
  C: Student KÃ¥re Thomassen, født: 19801010
  D: Student Ole Andreas Haug, født: 19801010
  
   */