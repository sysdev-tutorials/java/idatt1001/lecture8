class Student3 {
    private Name name;
    private int bday;
  
    public Student3(Name name, int bday) {
      /* Vi lar studentobjektet få sin egen kopi av navneobjektet */
      this.name = new Name(name.getFirstname(), name.getSurname());
      this.bday = bday;
    }
  
    public int getBdDay() {
      return bday;
    }
  
    public Name getName() {
      /* Returnerer en kopi */
      return new Name(name.getFirstname(), name.getSurname());
    }
    public void setName(Name newValue) {
      /* Vi lager en kopi av det navneobjektet som kommer inn */
      String newFirstname = newValue.getFirstname();
      String newSurname = newValue.getSurname();
      name.setFirstname(newFirstname);
      name.setSurname(newSurname);
    }
  
    public String toString() {
      return name + ", født: " + bday;
    }
  }
  
  class TestStudent3 {
    /*
     * Fokuserer på å vise at et studentobjekt har sitt eget navneobjekt,
     * som klienten ikke har tilgang til.
     */
    public static void main(String[] args) {
      /* Oppretter et navneobjekt som er argument til Student-konstruktøren */
      Name studName = new Name("Ole Andreas", "Thomassen");
  
      Student3 student = new Student3(studName, 19801010);
  
      /* Kontrollerer at navnet ble rett registrert. */
      System.out.println("A: Student " + student.toString());
  
      /* Endrer klienten sitt navneobjekt */
      studName.setFirstname("Ingolf");
  
      /* Studentobjektet skal ikke være endret */
      System.out.println("B: Student " + student.toString());
  
      /* Klienten henter ut (sin egen kopi av) studentobjektets navn */
      Name myName = student.getName();
      myName.setFirstname("Kåre");  // endrer klientens kopi
      System.out.println("C: Student " + student.toString());
  
      /* Endrer studentobjektets navn */
      student.setName(new Name("Ole Andreas", "Haug"));
      System.out.println("D: Student " + student.toString());
    }
  }
  
  /* Kjøring av programmet:
  A: Student Ole Andreas Thomassen, født: 19801010
  B: Student Ole Andreas Thomassen, født: 19801010
  C: Student Ole Andreas Thomassen, født: 19801010
  D: Student Ole Andreas Haug, født: 19801010
  */