/*
 * FInn etternavn??
 * Alt. a
 */


import java.util.Scanner;

class Student2 {
  private Name name;
  private int bday;

  public Student2(String startFirstname, String startSurname, int bday) {
    name = new Name(startFirstname, startSurname);
    this.bday = bday;
  }

  public int getBday() {
    return bday;
  }

  public String getFirstname() {
    return name.getFirstname();
  }

  public String getSurname() {
    return name.getSurname();
  }

  public void setFirstname(String newValue) {
    name.setFirstname(newValue);
  }

  public void setSurname(String newValue) {
    name.setSurname(newValue);
  }

  public String toString() {
    return name + ", født: " + bday;
  }

}

class TestStudent2 {
  public static void main(String[] args) {
    Student2 student = new Student2("Ole Andreas", "Thomassen", 19801010);
  
    System.out.println("Studenten heter " + student.getFirstname() + " " + student.getSurname());

    System.out.print("Oppgi nytt fornavn: ");
    String newFirstname = new Scanner(System.in).nextLine();
    System.out.print("Oppgi nytt etternavn: ");
    String newSurname = new Scanner(System.in).nextLine();
    student.setFirstname(newFirstname);
    student.setSurname(newSurname);

    System.out.println("Studenten heter nå " + student.toString());
  }
}