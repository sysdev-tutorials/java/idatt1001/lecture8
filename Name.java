public class Name {
    private String firstname;
    private String surname;

    public Name (String firstname, String surname) {
      this.firstname = firstname;
      this.surname = surname;
    }
    public void setFirstname(String newValue) {
      firstname = newValue;
    }
    public void setSurname(String newValue) {
      surname = newValue;
    }
    public String getFirstname() {
        return firstname;
      }
    public String getSurname() {
        return surname;
      }
    public String toString() {  
        return firstname + " " + surname;
      }
}
