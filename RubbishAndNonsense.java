public class RubbishAndNonsense {
    public void swapNumbers(int t1, int t2) {
        int hjelp = t1;
        t1 = t2;
        t2= hjelp;
    }

    public void swapObjects1 (Name f1, Name f2){
        Name help = f1;
        f1 = f2;
        f2 = help;
    }

    public void swapObjects2 (Name f1, Name f2){
        Name help = new Name (f1.getFirstname(), f1.getSurname());
        f1 = new Name(f2.getFirstname(), f2.getSurname());
        f2 = help;
    }

    public void swapObjects3 (Name f1, Name f2){
        Name help = f1;
        f1.setFirstname(f2.getFirstname());
        f1.setSurname(f2.getSurname());
        f2.setFirstname(help.getFirstname()); 
        f2.setSurname(help.getSurname());
    }

    public void swapObjects4 (Name f1, Name f2){
        Name help = new Name(f1.getFirstname(), f1.getSurname());
        f1.setFirstname(f2.getFirstname());
        f1.setSurname(f2.getSurname());
        f2.setFirstname(help.getFirstname()); 
        f2.setSurname(help.getSurname());
    }
}
