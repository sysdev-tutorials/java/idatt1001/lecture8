class Student{
	private String name;
	private int bday;

	public Student(String name, int bday){
		this.name = name;
		this.bday = bday;
	}

	public String getName(){
		return name;
	}

	public int getBday(){
		return bday;
	}

	public String toString(){
		return name + " " + bday;
	}
}

class TestStudent{
	public static void main(String[] args){

		String name = "Grethe Olsen";

		Student studentA = new Student(name, 19801010);

		System.out.println("student name: " + studentA.getName());
	}

}

