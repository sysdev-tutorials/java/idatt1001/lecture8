class Plane {
    
    private String name;
    private double length;
    private double width;
    private final double tolerance = 0.00001;

   public Plane (String name, double length, double width) {
        this.name = name;
        this.length = length;
        this.width = width;
   }

   public String getName () {
    return name;
   } 

   public double getCicumference (){
        return 2 * (length + width);
   }

   public boolean compareCircumference( Plane anotherPlane) {
        double circumference1 = getCicumference();
        double circumference2 = anotherPlane.getCicumference();
        if(Math.abs(circumference2 -circumference1) < tolerance) return true;
        return false;
   }

   @Override
   public boolean equals(Object obj) {
       return super.equals(obj);
   }

}

public class PlainClient {
     public static void main(String[] args) {
          new PlainClient().oppgage_compare();
          new PlainClient().oppgage_compare_with_equals();
     }

     private void oppgage_compare() {
          System.out.println("oppgage_compare");
          Plane plane1 = new Plane("A", 5, 4);
          Plane plane2 = new Plane("B", 7, 2);
          boolean isEqual = plane1.compareCircumference(plane2);
          System.out.println(isEqual? "A and B are equal" : "A and B are not equal");
      }
  
      private void oppgage_compare_with_equals() {
          System.out.println("oppgage_compare_with_equals");
          Plane plane1 = new Plane("A", 5, 4);
          Plane plane2 = new Plane("B", 7, 2);
          boolean isEqual = plane1.equals(plane2);
          System.out.println(isEqual? "A and B are equal" : "A and B are not equal");
          // why oppgage_compare -> true and oppgage_compare_with_equals > false, for same two objects?
  
          // equals method by default compares if two objects have same reference (in memory)
          // note that this behavior can be overiden/customized
      }
}
